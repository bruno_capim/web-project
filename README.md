# Projeto Web - Implementando a lógica do dado no arduino

Projeto com Arduino:


```c
Site utlizado para construir o arduino:
    -   Tinkercad   - 
(https://www.tinkercad.com)
```
```c
Acesse o site:
https://bruno_capim.gitlab.io/web-project/
```
## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/10042851/avatar.png?width=400) | Bruno Antunes | [@Bruno_Capim](https://gitlab.com/bruno_capim) | [loginobsequio@gmail.com](mailto:loginobsequio@gmail.com)
